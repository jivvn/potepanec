module ApplicationHelper
  # page title
  def full_title(page_title)
    base_title = 'BIGBAG Store'
    if page_title.blank?
      "#{base_title}"
    else
      "#{page_title} - #{base_title}"
    end
  end

  def product_image(product)
    if product.master.images.present?
      image_tag(product.master.images.first.attachment(:large), alt: "#{product.name}-img")
    else
      image_tag("img/products/products-01.jpg", alt: "#{product.name}-img")
    end
  end
end
