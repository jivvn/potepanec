module Potepan::ProductDecorator
  def self.prepended(base)
    # variant  Spree::Variantクラス
    # max_related_products 関連商品の最大表示数
    base.scope :init_related_products, ->(variant, max_related_products) do
      base.includes([master: [:images, :default_price]]).
        in_taxons(variant.product.taxon_ids).
        distinct.where.not(id: variant.id).sample(max_related_products)
    end
  end
  Spree::Product.prepend self
end
