class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:taxon_id])
    @taxon_roots = Spree::Taxon.roots
    @taxon_all_products = @taxon.all_products.includes([master: [:images, :default_price]])
  end
end
