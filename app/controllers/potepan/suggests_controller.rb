class Potepan::SuggestsController < ApplicationController
  def index
    search_params = params[:keyword]

    if bad_request?(search_params)
      render(json: { message: "search word error" }, status: 400)
      return
    end

    client = HTTPClient.new
    url = ENV['SEARCH_WEBAPI_URL']
    header = { 'Authorization' => ENV['SEARCH_WEBAPI_KEY'] }
    query = {
      'keyword' => search_params,
      'max_num' => Potepan::MAX_SUGGESTS_NUM,
    }
    res = client.get(url, query, header)
    if res.status == 200
      render json: JSON.parse(res.body)
    else
      render json: { message: "#{url} API error" }, status: res.status
    end
  end

  private

  def bad_request?(search_params)
    search_params.empty?
  end
end
