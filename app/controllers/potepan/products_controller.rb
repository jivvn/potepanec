class Potepan::ProductsController < ApplicationController
  def show
    @variant = Spree::Product.find(params[:id]).variants_including_master.find(params[:id])

    @default_taxon_id = Spree::Taxon.first.presence&.id

    @related_products = Spree::Product.init_related_products(@variant, Potepan::MAX_RELATED_PRODUCTS)
  end
end
