require 'rails_helper'
require 'webmock/rspec'

RSpec.describe Potepan::ProductsController, type: :request do
  describe "GET potepan/products/:id" do
    let!(:taxon_root) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon_root]) }
    let!(:lonely_taxon_root) { create(:taxon) }
    let!(:lonely_product) { create(:product, taxons: [lonely_taxon_root]) }

    describe 'アクセスに成功する' do
      before do
        get potepan_product_path product.id
      end

      it "responds successfully" do
        expect(response).to have_http_status(:successful)
      end
    end

    describe "関連商品のテスト" do
      describe '同じカテゴリの関連商品が表示数以上ある' do
        before do
          @related_products = create_list(:product, Potepan::MAX_RELATED_PRODUCTS + 1,
                                          taxons: [taxon_root])
          get potepan_product_path product.id
        end

        it '商品名タグが表示数分存在する' do
          within('.row.productsContent') do
            expect(page).to have_css "h5",
                                     count: Potepan::MAX_RELATED_PRODUCTS
          end
        end

        it 'いずれか商品が表示数分表示される' do
          within('.row.productsContent') do
            related_products_names = @related_products.map { |product| product.name }
            Potepan::MAX_RELATED_PRODUCTS.times do |num|
              h5_product_name = all('h5')[num].text
              expect(h5_product_name).to be_in(related_products_names)
            end
          end
        end

        it '商品の価格が表示数分表示される' do
          within('.row.productsContent') do
            expect(page).to have_css "h3",
                                     count: Potepan::MAX_RELATED_PRODUCTS
          end
        end

        it '商品画像用クラスが表示数分存在する' do
          within('.row.productsContent') do
            expect(page).to have_css ".productImage.clearfix",
                                     count: Potepan::MAX_RELATED_PRODUCTS
          end
        end

        it '違うカテゴリの商品名は表示されない' do
          within('.row.productsContent') do
            expect(page).not_to have_content lonely_product
          end
        end

        it "メイン商品名は表示されない" do
          within('.row.productsContent') do
            expect(page).not_to have_content product.name
          end
        end
      end

      describe '同じカテゴリの関連商品が他に1つだけ存在する' do
        let!(:registered_product) { create(:product, taxons: [taxon_root]) }

        before do
          get potepan_product_path product.id
        end

        it '商品名タグが1つだけ存在する' do
          within('.row.productsContent') do
            expect(page).to have_css "h5", count: 1
          end
        end

        it '商品が1つ表示される' do
          within('.row.productsContent') do
            expect(page).to have_content(registered_product.name)
          end
        end

        it '商品の価格が1つだけ表示される' do
          within('.row.productsContent') do
            expect(page).to have_css "h3",
                                     count: 1
          end
        end

        it '商品画像用クラスが1つだけ存在する' do
          within('.row.productsContent') do
            expect(page).to have_css ".productImage.clearfix",
                                     count: 1
          end
        end

        it '違うカテゴリの商品名は表示されない' do
          within('.row.productsContent') do
            expect(page).not_to have_content lonely_product
          end
        end

        it "メイン商品名は表示されない" do
          within('.row.productsContent') do
            expect(page).not_to have_content product.name
          end
        end
      end

      describe '同じカテゴリの関連商品が他に存在しない' do
        before do
          get potepan_product_path lonely_product.id
        end

        it '商品名タグが存在しない' do
          within('.row.productsContent') do
            expect(page).not_to have_css "h5"
          end
        end

        it '商品の価格が表示されない' do
          within('.row.productsContent') do
            expect(page).not_to have_css "h3"
          end
        end

        it '商品画像用クラスが存在しない' do
          within('.row.productsContent') do
            expect(page).not_to have_css ".productImage.clearfix"
          end
        end

        it '違うカテゴリの商品名は表示されない' do
          within('.row.productsContent') do
            expect(page).not_to have_content lonely_product
          end
        end

        it "メイン商品名は表示されない" do
          within('.row.productsContent') do
            expect(page).not_to have_content product.name
          end
        end
      end
    end

    describe 'APIテスト' do
      let(:keyword) { "ruby" }
      let(:max_num) { 5 }

      before do
        WebMock.enable!
        WebMock.disable_net_connect!(allow_localhost: true)

        stub_request(:get, ENV['SEARCH_WEBAPI_URL']).
          with(
            headers: { 'Authorization' => ENV['SEARCH_WEBAPI_KEY'] },
            query: { 'keyword' => 'ruby', 'max_num' => 5 }
          ).
          to_return(
            status: 200,
            body: '["rspec","webmock","potepan","abcdefg","hijklmnop"]'
          )
      end

      it 'リクエストに成功する' do
        get potepan_suggests_path, params: { keyword: keyword, max_num: max_num }
        expect(response).to have_http_status(200)
      end

      context '検索ワードが与えられた時' do
        it '正しいレスポンスが帰ってくる' do
          get potepan_suggests_path, params: { keyword: keyword, max_num: max_num }
          expect(body).to eq '["rspec","webmock","potepan","abcdefg","hijklmnop"]'
        end
      end

      context '検索ワードが空文字の時' do
        let(:keyword) { "" }
        let(:max_num) { 5 }

        it '400エラーを返す' do
          get potepan_suggests_path, params: { keyword: keyword, max_num: max_num }
          expect(response).to have_http_status(400)
        end

        it '外部APIが呼ばれない' do
          get potepan_suggests_path, params: { keyword: keyword, max_num: max_num }
          expect(a_request(:get, ENV['SEARCH_WEBAPI_URL'])).not_to have_been_made.once
        end
      end
    end
  end
end
