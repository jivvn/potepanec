require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :request do
  describe "GET potepan/categories/:taxon_id" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, name: "rubyproduct", taxons: [taxon]) }
    let(:another_taxon_product) { create(:product, name: "another_taxon_product", price: 999.99) }

    before do
      get potepan_category_path taxon.id
    end

    it "アクセスに成功する" do
      expect(response).to have_http_status(:successful)
    end

    it "ページタイトルが正しく表示される" do
      expect(response.body).to include "#{taxon.name} - BIGBAG Store"
    end

    it 'カテゴリ名が表示される' do
      expect(response.body).to include taxon.name
    end

    it '"商品カテゴリー"が表示される' do
      expect(response.body).to include '商品カテゴリー'
    end

    it '"色から探す"が表示される' do
      expect(response.body).to include '色から探す'
    end

    it '"サイズから探す"が表示される' do
      expect(response.body).to include 'サイズから探す'
    end

    describe '同じカテゴリのproductデータが表示される' do
      it '商品名が表示される' do
        expect(response.body).to include product.name
      end

      it '商品価格が表示される' do
        expect(response.body).to include product.display_price.to_s
      end

      it '商品画像用クラスが存在する' do
        expect(response.body).to include 'alt="' + product.name + '-img"'
      end
    end

    describe '違うカテゴリのproductデータは表示されない' do
      it '商品名が表示されない' do
        expect(response.body).not_to include another_taxon_product.name
      end

      it '商品価格が表示されない' do
        expect(response.body).not_to include another_taxon_product.display_price.to_s
      end

      it '商品画像用クラスが存在しない' do
        expect(response.body).not_to include 'alt="' + another_taxon_product.name + '-img"'
      end
    end
  end
end
