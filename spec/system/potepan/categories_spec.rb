require 'rails_helper'

RSpec.describe 'Categories page system spec', type: :system do
  describe 'カテゴリーページ' do
    let!(:taxonomy_brand) { create(:taxonomy, name: 'taxonomy_brand') }
    let!(:taxon_root) { create(:taxon, taxonomy: taxonomy_brand) }
    let!(:taxon_leaf) { create(:taxon, name: 'Rails Leaf', taxonomy: taxonomy_brand, parent_id: taxon_root.id) }

    let!(:product) { create(:product, name: "rubyproduct", taxons: [taxon_root]) }

    before do
      visit potepan_category_path taxon_root.id
    end

    it 'ルートカテゴリ名が表示される' do
      within('.nav.navbar-nav.side-nav') do
        Spree::Taxon.roots.each do |root|
          expect(page).to have_content root.name
        end
      end
    end

    it 'サブカテゴリ名が表示される' do
      within('.nav.navbar-nav.side-nav') do
        Spree::Taxon.leaves.each do |leaf|
          expect(page).to have_content leaf.name
        end
      end
    end

    it 'トップページに飛べる' do
      page.first(".navbar-brand").click
      expect(current_path).to eq potepan_index_path
    end

    it '商品詳細ページに飛べる' do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    describe '同じカテゴリーの商品' do
      it '商品名がリンクとして表示される' do
        expect(page).to have_link product.name
      end

      it '商品価格がリンクとして表示される' do
        expect(page).to have_link product.display_price.to_s
      end
    end

    describe '別のカテゴリーの商品' do
      let!(:another_taxonomy_brand) { create(:taxonomy, name: 'another_taxonomy_brand') }
      let!(:another_taxon_root) { create(:taxon, taxonomy: another_taxonomy_brand) }
      let!(:another_product) do
        create(:product, name: 'another_rubyproduct', taxons: [another_taxon_root], price: 999.99)
      end

      it '商品名がリンクとして表示されない' do
        expect(page).not_to have_link another_product.name
      end

      it '商品価格がリンクとして表示されない' do
        expect(page).not_to have_link another_product.display_price.to_s
      end
    end
  end
end
