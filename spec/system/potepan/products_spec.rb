require 'rails_helper'

FactoryBot.define do
  RSpec.describe 'Products page system spec', type: :system do
    describe '商品詳細情報' do
      describe 'UI全般' do
        let!(:taxon_root) { create(:taxon) }
        let!(:product) { create(:product, taxons: [taxon_root]) }

        before do
          visit potepan_product_path product.id
        end

        it 'display「カートへ入れる」' do
          expect(page.body).to include 'カートへ入れる'
        end

        it 'display name' do
          expect(page.body).to include product.name
        end

        it 'display price' do
          expect(page.body).to include product.display_price.to_s
        end

        it 'display navbar-brand link' do
          expect(page.body).to include 'class="navbar-brand" href="/potepan/index"'
        end
      end

      describe 'productにtaxonが登録されてない、かつSpree::Taxon.firstが存在しないときの商品詳細ページ' do
        let(:unregistered_product) { create(:product) }

        before do
          visit potepan_product_path unregistered_product.id
        end

        it 'トップページに移動できること' do
          click_link 'トップページへ戻る'
          expect(current_path).to eq potepan_index_path
        end
      end

      describe 'productにtaxonが登録されてないがSpree::Taxon.firstは存在するときの商品詳細ページ' do
        let!(:taxon_root) { create(:taxon) }
        let!(:unregistered_product) { create(:product) }

        before do
          visit potepan_product_path unregistered_product.id
        end

        it '一覧ページへ戻るが表示される' do
          expect(page).to have_link '一覧ページへ戻る'
        end

        it 'Spree::Taxon.firstのカテゴリーページに移動できること' do
          click_link '一覧ページへ戻る'
          expect(current_path).to eq potepan_category_path Spree::Taxon.first.id
        end
      end

      describe 'productにtaxonが登録されている商品詳細ページ' do
        let(:taxon_root) { create(:taxon) }
        let!(:registered_product) { create(:product, taxons: [taxon_root]) }

        before do
          visit potepan_product_path registered_product.id
        end

        it '一覧ページへ戻るが表示される' do
          expect(page).to have_link '一覧ページへ戻る'
        end

        it '商品のカテゴリーページに移動できること' do
          click_link '一覧ページへ戻る'
          expect(current_path).to eq potepan_category_path registered_product.taxons.first.id
        end
      end
    end

    describe '関連商品情報' do
      let(:taxon_root) { create(:taxon) }
      let!(:product) { create(:product, taxons: [taxon_root]) }
      let!(:related_products) { create(:product, taxons: [taxon_root]) }

      before do
        visit potepan_product_path product.id
      end

      it '関連商品名がリンクとして表示される' do
        within('.row.productsContent') do
          expect(page).to have_link related_products.name
        end
      end

      it '商品名をクリックすると関連商品の詳細ページに移動できること' do
        within('.row.productsContent') do
          click_link related_products.name
          expect(current_path).to eq potepan_product_path related_products.id
        end
      end

      it '関連商品価格がリンクとして表示される' do
        within('.row.productsContent') do
          expect(page).to have_link related_products.display_price.to_s
        end
      end

      it '商品価格をクリックすると関連商品の詳細ページに移動できること' do
        within('.row.productsContent') do
          click_link related_products.display_price.to_s
          expect(current_path).to eq potepan_product_path related_products.id
        end
      end

      it '商品画像をクリックすると関連商品の詳細ページに移動できること' do
        within('.row.productsContent') do
          click_on "#{related_products.name}-img"
          expect(current_path).to eq potepan_product_path related_products.id
        end
      end
    end
  end
end
